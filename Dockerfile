FROM ruby:2.6.3

RUN apt-get update && apt-get install -y \
  curl \
  build-essential \
  libpq-dev &&\
  curl -sL https://deb.nodesource.com/setup_10.x | bash - && \
  curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - && \
  echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list && \
  apt-get update && apt-get install -y nodejs yarn

RUN mkdir /app
WORKDIR /app

EXPOSE 3000

COPY Gemfile .
COPY Gemfile.lock .
RUN gem update bundler
RUN bundle install --jobs 5

COPY package.json .
COPY yarn.lock .
RUN yarn install


# FROM ruby:2.6.3

# RUN apt-get update -qq && apt-get install -y curl build-essential libpq-dev 

# RUN mkdir /app
# RUN mkdir -p /usr/local/nvm
# WORKDIR /app

# RUN curl -sL https://deb.nodesource.com/setup_11.x | bash -
# RUN apt-get install -y nodejs
# RUN node -v
# RUN npm -v

# ENV BUNDLE_PATH /gems

# RUN gem install bundler -v 2.0.2

# COPY Gemfile Gemfile.lock package.json yarn.lock ./

# RUN bundle install

# RUN npm install -g yarn

# RUN yarn install
