# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...

# Running Locally from Docker 

* docker-compose build

* docker-compose run myweb bundle install

* docker-compose run myweb yarn install

* docker-compose run myweb rails db:migrate

* docker-compose up

* localhost:30001/<api_endpoint>

* docker-compose down