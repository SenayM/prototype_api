class CustomerTestimoniesController < ApplicationController
  before_action :set_customer_testimony, only: %i[ show edit update destroy ]

  # GET /customer_testimonies or /customer_testimonies.json
  def index
    @customer_testimonies = CustomerTestimony.all
  end

  # GET /customer_testimonies/1 or /customer_testimonies/1.json
  def show
  end

  # GET /customer_testimonies/new
  def new
    @customer_testimony = CustomerTestimony.new
  end

  # GET /customer_testimonies/1/edit
  def edit
  end

  # POST /customer_testimonies or /customer_testimonies.json
  def create
    @customer_testimony = CustomerTestimony.new(customer_testimony_params)

    respond_to do |format|
      if @customer_testimony.save
        format.html { redirect_to @customer_testimony, notice: "Customer testimony was successfully created." }
        format.json { render :show, status: :created, location: @customer_testimony }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @customer_testimony.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /customer_testimonies/1 or /customer_testimonies/1.json
  def update
    respond_to do |format|
      if @customer_testimony.update(customer_testimony_params)
        format.html { redirect_to @customer_testimony, notice: "Customer testimony was successfully updated." }
        format.json { render :show, status: :ok, location: @customer_testimony }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @customer_testimony.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /customer_testimonies/1 or /customer_testimonies/1.json
  def destroy
    @customer_testimony.destroy
    respond_to do |format|
      format.html { redirect_to customer_testimonies_url, notice: "Customer testimony was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_customer_testimony
      @customer_testimony = CustomerTestimony.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def customer_testimony_params
      params.require(:customer_testimony).permit(:testimony_logo, :testimony_detail, :testimony_cust_img, :testimony_cust_name, :testimony_cust_title)
    end
end
