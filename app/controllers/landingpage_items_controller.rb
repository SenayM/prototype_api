class LandingpageItemsController < ApplicationController
  before_action :set_landingpage_item, only: %i[ show edit update destroy ]

  # GET /landingpage_items or /landingpage_items.json
  def index
    @landingpage_items = LandingpageItem.all
  end

  # GET /landingpage_items/1 or /landingpage_items/1.json
  def show
  end

  # GET /landingpage_items/new
  def new
    @landingpage_item = LandingpageItem.new
  end

  # GET /landingpage_items/1/edit
  def edit
  end

  # POST /landingpage_items or /landingpage_items.json
  def create
    @landingpage_item = LandingpageItem.new(landingpage_item_params)

    respond_to do |format|
      if @landingpage_item.save
        format.html { redirect_to @landingpage_item, notice: "Landingpage item was successfully created." }
        format.json { render :show, status: :created, location: @landingpage_item }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @landingpage_item.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /landingpage_items/1 or /landingpage_items/1.json
  def update
    respond_to do |format|
      if @landingpage_item.update(landingpage_item_params)
        format.html { redirect_to @landingpage_item, notice: "Landingpage item was successfully updated." }
        format.json { render :show, status: :ok, location: @landingpage_item }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @landingpage_item.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /landingpage_items/1 or /landingpage_items/1.json
  def destroy
    @landingpage_item.destroy
    respond_to do |format|
      format.html { redirect_to landingpage_items_url, notice: "Landingpage item was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_landingpage_item
      @landingpage_item = LandingpageItem.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def landingpage_item_params
      params.require(:landingpage_item).permit(:name, :description, :price, :rating)
    end
end
