class CustomerTestimony
  include Mongoid::Document
  include Mongoid::Timestamps
  field :testimony_logo, type: String
  field :testimony_detail, type: String
  field :testimony_cust_img, type: String
  field :testimony_cust_name, type: String
  field :testimony_cust_title, type: String
end
