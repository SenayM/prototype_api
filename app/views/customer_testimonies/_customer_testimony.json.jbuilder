json.extract! customer_testimony, :id, :testimony_logo, :testimony_detail, :testimony_cust_img, :testimony_cust_name, :testimony_cust_title, :created_at, :updated_at
json.url customer_testimony_url(customer_testimony, format: :json)
