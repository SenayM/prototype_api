json.extract! landingpage_item, :id, :name, :description, :price, :rating, :created_at, :updated_at
json.url landingpage_item_url(landingpage_item, format: :json)
