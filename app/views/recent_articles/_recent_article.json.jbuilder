json.extract! recent_article, :id, :article_img, :publication_date, :publisher, :title, :created_at, :updated_at
json.url recent_article_url(recent_article, format: :json)
