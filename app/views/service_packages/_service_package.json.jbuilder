json.extract! service_package, :id, :logo, :title, :detail, :created_at, :updated_at
json.url service_package_url(service_package, format: :json)
