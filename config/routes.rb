Rails.application.routes.draw do
  resources :recent_articles
  resources :customer_testimonies
  resources :pricing_plans
  resources :service_packages
  resources :landingpage_items
  resources :featured_products
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
