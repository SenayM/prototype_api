require 'test_helper'

class CustomerTestimoniesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @customer_testimony = customer_testimonies(:one)
  end

  test "should get index" do
    get customer_testimonies_url
    assert_response :success
  end

  test "should get new" do
    get new_customer_testimony_url
    assert_response :success
  end

  test "should create customer_testimony" do
    assert_difference('CustomerTestimony.count') do
      post customer_testimonies_url, params: { customer_testimony: { testimony_cust_img: @customer_testimony.testimony_cust_img, testimony_cust_name: @customer_testimony.testimony_cust_name, testimony_cust_title: @customer_testimony.testimony_cust_title, testimony_detail: @customer_testimony.testimony_detail, testimony_logo: @customer_testimony.testimony_logo } }
    end

    assert_redirected_to customer_testimony_url(CustomerTestimony.last)
  end

  test "should show customer_testimony" do
    get customer_testimony_url(@customer_testimony)
    assert_response :success
  end

  test "should get edit" do
    get edit_customer_testimony_url(@customer_testimony)
    assert_response :success
  end

  test "should update customer_testimony" do
    patch customer_testimony_url(@customer_testimony), params: { customer_testimony: { testimony_cust_img: @customer_testimony.testimony_cust_img, testimony_cust_name: @customer_testimony.testimony_cust_name, testimony_cust_title: @customer_testimony.testimony_cust_title, testimony_detail: @customer_testimony.testimony_detail, testimony_logo: @customer_testimony.testimony_logo } }
    assert_redirected_to customer_testimony_url(@customer_testimony)
  end

  test "should destroy customer_testimony" do
    assert_difference('CustomerTestimony.count', -1) do
      delete customer_testimony_url(@customer_testimony)
    end

    assert_redirected_to customer_testimonies_url
  end
end
