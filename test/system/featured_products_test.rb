require "application_system_test_case"

class FeaturedProductsTest < ApplicationSystemTestCase
  setup do
    @featured_product = featured_products(:one)
  end

  test "visiting the index" do
    visit featured_products_url
    assert_selector "h1", text: "Featured Products"
  end

  test "creating a Featured product" do
    visit featured_products_url
    click_on "New Featured Product"

    fill_in "Img title", with: @featured_product.img_title
    fill_in "Img url", with: @featured_product.img_url
    fill_in "Product name", with: @featured_product.product_name
    click_on "Create Featured product"

    assert_text "Featured product was successfully created"
    click_on "Back"
  end

  test "updating a Featured product" do
    visit featured_products_url
    click_on "Edit", match: :first

    fill_in "Img title", with: @featured_product.img_title
    fill_in "Img url", with: @featured_product.img_url
    fill_in "Product name", with: @featured_product.product_name
    click_on "Update Featured product"

    assert_text "Featured product was successfully updated"
    click_on "Back"
  end

  test "destroying a Featured product" do
    visit featured_products_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Featured product was successfully destroyed"
  end
end
