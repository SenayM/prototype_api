require "application_system_test_case"

class LandingpageItemsTest < ApplicationSystemTestCase
  setup do
    @landingpage_item = landingpage_items(:one)
  end

  test "visiting the index" do
    visit landingpage_items_url
    assert_selector "h1", text: "Landingpage Items"
  end

  test "creating a Landingpage item" do
    visit landingpage_items_url
    click_on "New Landingpage Item"

    fill_in "Description", with: @landingpage_item.description
    fill_in "Name", with: @landingpage_item.name
    fill_in "Price", with: @landingpage_item.price
    fill_in "Rating", with: @landingpage_item.rating
    click_on "Create Landingpage item"

    assert_text "Landingpage item was successfully created"
    click_on "Back"
  end

  test "updating a Landingpage item" do
    visit landingpage_items_url
    click_on "Edit", match: :first

    fill_in "Description", with: @landingpage_item.description
    fill_in "Name", with: @landingpage_item.name
    fill_in "Price", with: @landingpage_item.price
    fill_in "Rating", with: @landingpage_item.rating
    click_on "Update Landingpage item"

    assert_text "Landingpage item was successfully updated"
    click_on "Back"
  end

  test "destroying a Landingpage item" do
    visit landingpage_items_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Landingpage item was successfully destroyed"
  end
end
