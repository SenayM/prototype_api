require "application_system_test_case"

class RecentArticlesTest < ApplicationSystemTestCase
  setup do
    @recent_article = recent_articles(:one)
  end

  test "visiting the index" do
    visit recent_articles_url
    assert_selector "h1", text: "Recent Articles"
  end

  test "creating a Recent article" do
    visit recent_articles_url
    click_on "New Recent Article"

    fill_in "Article img", with: @recent_article.article_img
    fill_in "Publication date", with: @recent_article.publication_date
    fill_in "Publisher", with: @recent_article.publisher
    fill_in "Title", with: @recent_article.title
    click_on "Create Recent article"

    assert_text "Recent article was successfully created"
    click_on "Back"
  end

  test "updating a Recent article" do
    visit recent_articles_url
    click_on "Edit", match: :first

    fill_in "Article img", with: @recent_article.article_img
    fill_in "Publication date", with: @recent_article.publication_date
    fill_in "Publisher", with: @recent_article.publisher
    fill_in "Title", with: @recent_article.title
    click_on "Update Recent article"

    assert_text "Recent article was successfully updated"
    click_on "Back"
  end

  test "destroying a Recent article" do
    visit recent_articles_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Recent article was successfully destroyed"
  end
end
