require "application_system_test_case"

class ServicePackagesTest < ApplicationSystemTestCase
  setup do
    @service_package = service_packages(:one)
  end

  test "visiting the index" do
    visit service_packages_url
    assert_selector "h1", text: "Service Packages"
  end

  test "creating a Service package" do
    visit service_packages_url
    click_on "New Service Package"

    fill_in "Detail", with: @service_package.detail
    fill_in "Logo", with: @service_package.logo
    fill_in "Title", with: @service_package.title
    click_on "Create Service package"

    assert_text "Service package was successfully created"
    click_on "Back"
  end

  test "updating a Service package" do
    visit service_packages_url
    click_on "Edit", match: :first

    fill_in "Detail", with: @service_package.detail
    fill_in "Logo", with: @service_package.logo
    fill_in "Title", with: @service_package.title
    click_on "Update Service package"

    assert_text "Service package was successfully updated"
    click_on "Back"
  end

  test "destroying a Service package" do
    visit service_packages_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Service package was successfully destroyed"
  end
end
